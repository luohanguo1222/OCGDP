Pod::Spec.new do |s|
  s.name         = 'OCGDP'
  s.version      = '0.0.8'
  s.summary      = '常用OC分类和工具类(A collection of OC components.)'
  s.homepage     = 'https://gitee.com/luohanguo1222/OCGDP'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'luohanguo' => 'wy335089101@163.com' }
  s.platform     = :ios
  s.ios.deployment_target = '7.0'
  s.source       = { :git => 'https://gitee.com/luohanguo1222/OCGDP.git', :tag => s.version.to_s }
  s.requires_arc = true
  s.source_files = 'OCGDP/**/*.{h,m}'
  s.public_header_files = 'OCGDP/**/*.{h}'
  s.frameworks = ['UIKit', 'Foundation']
end
