//
//  SceneDelegate.h
//  OCGDPDemo
//
//  Created by sun on 2022/7/28.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

