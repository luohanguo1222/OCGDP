//  Created by sunmumu

#import <Foundation/Foundation.h>

#if __has_include(<OCGDP/OCGDP.h>)

#import <OCGDP/NSString+GDP.h>
#import <OCGDP/NSDate+GDP.h>
#import <OCGDP/UIImage+GDP.h>
#import <OCGDP/UILabel+GDP.h>
#import <OCGDP/UIView+GDP.h>
#import <OCGDP/UITabBar+GDP.h>
#import <OCGDP/UIViewController+GDP.h>
#import <OCGDP/UIAlertController+GDP.h>
#import <OCGDP/GDPFileManager.h>
#import <OCGDP/UITextField+GDP.h>
#import <OCGDP/UITextField+NumberFormat.h>
//#import <OCGDP/UINavigationController+GDP.h>


#else

#import "NSString+GDP.h"
#import "NSDate+GDP.h"
#import "UIImage+GDP.h"
#import "UILabel+GDP.h"
#import "UIView+GDP.h"
#import "UITabBar+GDP.h"
#import "UITextField+GDP.h"
#import "UIViewController+GDP.h"
#import "UIAlertController+GDP.h"
#import "GDPFileManager.h"
#import "UITextField+NumberFormat.h"
//#import "UINavigationController+GDP.h"


//特殊说明
//UIView+ScrollToTop.h用法 :
//拖UIView+ScrollToTop.h / UIView+ScrollToTop.m文件到项目中, 不需要导入头文件.
//需要点击状态栏, 需要对应UIScrollView回滚最上面, 设置对应 scrollView.scrollsToTop = YES;


#endif
