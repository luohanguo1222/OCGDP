# OCGDP

#### 介绍
常用OC分类和工具类(A collection of OC components.)

#### 安装教程
cocoapods安装
pod 'OCGDP', :git => 'https://gitee.com/luohanguo1222/OCGDP.git'

本地拖入源文件安装
OCGDP文件夹拷贝到项目中

#### 使用说明
import <OCGDP/OCGDP.h>
或
import "OCGDP.h"
